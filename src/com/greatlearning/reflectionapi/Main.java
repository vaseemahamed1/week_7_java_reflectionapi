package com.greatlearning.reflectionapi;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        // input parameters
        Scanner input = new Scanner(System.in);
        Integer menuChoice;
        Integer inputChoice;
        Class classObj;

        System.out.printf("---- Please enter the class name ----\n");
        // get the class name
        classObj = ClassUtil.getValidClassInput(input);

        System.out.printf("---- Select the menu option ----\n");
        // display the class details based on user option selection
        menuChoice = ClassUtil.displayClassDetails(input, classObj);

        do {
            System.out.println("\n ----Do you want to see any other information----");
            System.out.println("  select 1.Yes / 2.No to recheck the menu or continue");
            // get input for next step
            inputChoice = input.nextInt();
            if (inputChoice == 1) {
                // display until user selects to continue with next option
                ClassUtil.displayClassDetails(input, classObj);
            }
            if (inputChoice != 1 && inputChoice != 2) {
                // check for invalid selection
                System.out.println("--invalid input, please select again--");
                inputChoice = 1;
            }
        } while (inputChoice == 1);

        System.out.println("1. Store latest information into file");
        System.out.println("2. To see all previous files Created");
        System.out.println("3. Exit without storing");
        // get the user input for file operation
        inputChoice = input.nextInt();
        switch (inputChoice) {
            case 1:
                FileUtil.storeDataToFile(classObj, menuChoice);
                break;
            case 2:
                FileUtil.retrieveStoredFileDetails();
                break;
            default:
                break;
        }
    }

}
