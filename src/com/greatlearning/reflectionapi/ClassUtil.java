package com.greatlearning.reflectionapi;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Scanner;

public class ClassUtil {

    static Class getValidClassInput(Scanner input) {
        String menuChoice;
        Class classObj = null;
        boolean inValidClass = true;
        while (inValidClass) {
            menuChoice = input.nextLine();
            try {
                // get the Class instance using reflection api
                classObj = Class.forName(menuChoice);
                inValidClass = false;
            } catch (ClassNotFoundException e) {
                // check for error notification
                System.out.printf("class provided is invalid, please provide a valid input\n");
            }
        }
        return classObj;
    }

    public static Integer displayClassDetails(Scanner input, Class classObj) {
        // provide option to view class details
        System.out.printf(" 1. Methods \n 2. Class \n 3. Subclasses \n 4. Parent classes  \n 5. Constructors \n 6. Data Members \n");
        boolean invalidInput;
        Integer inputChoice;
        do {
            invalidInput = false;
            // get use selection of the menu
            inputChoice = input.nextInt();
            switch (inputChoice) {
                case 1:
                    ClassUtil.displayMethodList(classObj);
                    break;
                case 2:
                    ClassUtil.displayClassList(classObj);
                    break;
                case 3:
                    ClassUtil.displaySubclassesList(classObj);
                    break;
                case 4:
                    ClassUtil.displayParentClasses(classObj);
                    break;
                case 5:
                    ClassUtil.displayConstructorList(classObj);
                    break;
                case 6:
                    ClassUtil.displayDataMembers(classObj);
                    break;
                default:
                    // check for invalid input and specify the message
                    System.out.printf("invalid input, please select again \n");
                    invalidInput = true;
                    break;
            }
        } while (invalidInput);
        return inputChoice;
    }

    public static void displayMethodList(Class classObj) {
        // get the method list details
        System.out.println("Method list :");
        Method methodList[] = classObj.getMethods();
        for (Method method : methodList) {
            System.out.println(method);
        }
    }

    public static void displayClassList(Class classObj) {
        // get the class details
        System.out.println("Class details :");
        System.out.println(classObj.getClass());
        System.out.println("package name: " + classObj.getPackage());
        System.out.println("class name: " + classObj.getName());
        System.out.println("class simple name: " + classObj.getSimpleName());
    }

    public static void displaySubclassesList(Class classObj) {
        // get the sub classes
        System.out.println("Subclass list :");
        System.out.println(classObj.getInterfaces());
    }

    public static void displayParentClasses(Class classObj) {
        // get the parent classes
        System.out.println("Parent class :");
        System.out.println("class complete name :" + classObj.getSuperclass().getName());
        System.out.println("class simple name :" + classObj.getSuperclass().getSimpleName());
    }

    public static void displayConstructorList(Class classObj) {
        // display all the constructors
        Constructor<?>[] constructorsList = classObj.getDeclaredConstructors();
        System.out.println("Constructor list :");
        // get the list of constructors
        for (Constructor constructor : constructorsList) {
            Parameter[] parameters = constructor.getParameters();
            System.out.print("\n" + classObj.getSimpleName() + "(");
            Integer count = 1;
            // get the parameter list for a constructor
            for (Parameter param : parameters) {
                System.out.print(param.getType().getSimpleName() + " " + param.getName());
                if (count != parameters.length) {
                    System.out.print(",");
                }
                count++;
            }
            System.out.print(")");
        }
    }

    public static void displayDataMembers(Class classObj) {
        // display all the field present in the class
        Field[] fields = classObj.getDeclaredFields();
        System.out.println("Fields list :");
        for (Field field : fields) {
            System.out.println(field.getType().getSimpleName() + " " + field.getName());
        }
    }
}
