package com.greatlearning.reflectionapi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Scanner;

public class FileUtil {
    public static Scanner input = new Scanner(System.in);

    public static void storeDataToFile(Class classObj, Integer inputChoice) throws IOException {
        // get the file name from user input
        System.out.println("-- please provide file name --");
        // define the file extension
        String fileName = input.nextLine() + ".txt";
        FileWriter writer;
        // get file writer instance
        writer = new FileWriter(fileName, true);
        switch (inputChoice) {
            case 1:
                saveMethodList(writer, classObj);
                break;
            case 2:
                saveClassList(writer, classObj);
                break;
            case 3:
                saveSubclassesList(writer, classObj);
                break;
            case 4:
                saveParentClasses(writer, classObj);
                break;
            case 5:
                saveConstructorList(writer, classObj);
                break;
            case 6:
                saveDataMembers(writer, classObj);
                break;
        }
        // close the connection to file writer
        writer.close();
        System.out.printf("data saved to file %s successfully\n", fileName);
    }

    private static void saveDataMembers(FileWriter writer, Class classObj) throws IOException {
        // save data members
        saveToFile(writer, "Fields list :");
        Field[] fields = classObj.getDeclaredFields();
        for (Field field : fields) {
            saveToFile(writer, field.getType().getSimpleName() + " " + field.getName());
        }
    }

    private static void saveConstructorList(FileWriter writer, Class classObj) throws IOException {
        // save constructors
        saveToFile(writer, "Constructor list :");
        Constructor<?>[] constructorsList = classObj.getDeclaredConstructors();
        for (Constructor constructor : constructorsList) {
            saveToFile(writer, constructor.toString());
        }
    }

    private static void saveParentClasses(FileWriter writer, Class classObj) throws IOException {
        // save parent class details
        saveToFile(writer, "Parent class :");
        saveToFile(writer, "name: " + classObj.getSuperclass().getName());
        saveToFile(writer, "simple name: " + classObj.getSuperclass().getSimpleName());
    }

    private static void saveSubclassesList(FileWriter writer, Class classObj) throws IOException {
        // save sub class details
        saveToFile(writer, "Subclass list :");
        saveToFile(writer, classObj.getInterfaces().toString());
    }

    private static void saveClassList(FileWriter writer, Class classObj) throws IOException {
        // save class list
        saveToFile(writer, "Class details :");
        saveToFile(writer, classObj.getClass().toString());
        saveToFile(writer, classObj.getPackage().toString());
        saveToFile(writer, classObj.getName());
        saveToFile(writer, classObj.getSimpleName());
    }

    private static void saveMethodList(FileWriter writer, Class classObj) throws IOException {
        // save method list
        Method methodList[] = classObj.getMethods();
        for (Method method : methodList) {
            saveToFile(writer, method.toString());
        }
    }

    public static void retrieveStoredFileDetails() {
        // retrive file details
        Boolean invalidFile = true;
        System.out.printf("\n--- list of file created -----\n");
        File file = new File("./");
        // get list of files from local and filter based on extension
        String[] pathnames = Arrays.stream(file.list()).filter(s -> s.contains(".txt")).toArray(String[]::new);
        for (String pathname : pathnames) {
            // display file names
            System.out.println(pathname);
        }
        System.out.printf("--- please provide file name to view the content -----\n");
        do {
            try {
                // get the use input for file name
                String fileName = input.nextLine();
                System.out.printf("---------- file details are below -----\n");
                File myObj = new File(fileName);
                Scanner myReader = new Scanner(myObj);
                while (myReader.hasNextLine()) {
                    // display file details
                    String data = myReader.nextLine();
                    System.out.println(data);
                }
                // close connection to file reader
                myReader.close();
                invalidFile = false;
                System.out.printf("--------------------------------------------\n");
            } catch (FileNotFoundException e) {
                System.out.printf("invalid file name, please provide correct name\n");
            }
        } while (invalidFile);

    }

    static void saveToFile(FileWriter writer, String data) throws IOException {
        // write connect to file with new line
        writer.write(data);
        writer.append('\n');
    }
}
